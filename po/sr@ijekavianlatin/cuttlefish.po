# Translation of cuttlefish.po into Serbian.
# Chusslove Illich <caslav.ilic@gmx.net>, 2015, 2016, 2017.
msgid ""
msgstr ""
"Project-Id-Version: cuttlefish\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2020-03-12 03:53+0100\n"
"PO-Revision-Date: 2017-05-07 22:20+0200\n"
"Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>\n"
"Language-Team: Serbian <kde-i18n-sr@kde.org>\n"
"Language: sr@ijekavianlatin\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"

#: package/contents/ui/Actions.qml:31
#, kde-format
msgid "Open icon with external program"
msgstr ""

#: package/contents/ui/Actions.qml:36
#, kde-format
msgid "Insert icon name"
msgstr "Unesite ime ikonice"

#: package/contents/ui/Actions.qml:36
#, kde-format
msgid "Copy icon name to clipboard"
msgstr "Kopiraj ime ikonice u klipbord"

#: package/contents/ui/Actions.qml:40
#, fuzzy, kde-format
#| msgid "Copy icon name to clipboard"
msgid "Icon name copied to clipboard"
msgstr "Kopiraj ime ikonice u klipbord"

#: package/contents/ui/Actions.qml:46
#, kde-format
msgid "Create screenshot of icon with..."
msgstr ""

#: package/contents/ui/Actions.qml:49
#, kde-format
msgid "Breeze Colors"
msgstr ""

#: package/contents/ui/Actions.qml:53
#, kde-format
msgid "Breeze Dark Colors"
msgstr ""

#: package/contents/ui/Actions.qml:58 package/contents/ui/GlobalMenuBar.qml:50
#, kde-format
msgid "Breeze (Normal) and Breeze Dark"
msgstr ""

#: package/contents/ui/Actions.qml:62 package/contents/ui/GlobalMenuBar.qml:38
#, kde-format
msgid "Active Color Scheme"
msgstr ""

#: package/contents/ui/Actions.qml:66
#, kde-format
msgid "View icon in other themes"
msgstr ""

#: package/contents/ui/GlobalMenuBar.qml:34
#, fuzzy, kde-format
#| msgid "Filename:"
msgid "File"
msgstr "Ime fajla:"

#: package/contents/ui/GlobalMenuBar.qml:36
#, kde-format
msgid "Export Montage with Color Scheme..."
msgstr ""

#: package/contents/ui/GlobalMenuBar.qml:42
#, kde-format
msgid "Breeze (Normal)"
msgstr ""

#: package/contents/ui/GlobalMenuBar.qml:46
#, kde-format
msgid "Breeze Dark"
msgstr ""

#: package/contents/ui/GlobalMenuBar.qml:57
#, kde-format
msgid "Quit"
msgstr ""

#: package/contents/ui/GlobalMenuBar.qml:63
#, kde-format
msgid "View"
msgstr ""

#: package/contents/ui/GlobalMenuBar.qml:66
#, kde-format
msgid "Zoom In"
msgstr ""

#: package/contents/ui/GlobalMenuBar.qml:72
#, kde-format
msgid "Zoom Out"
msgstr ""

#: package/contents/ui/Preview.qml:81
#: package/contents/ui/ResponsivePreview.qml:52
#, fuzzy, kde-format
#| msgid "Filename:"
msgid "File name:"
msgstr "Ime fajla:"

#: package/contents/ui/Preview.qml:86
#: package/contents/ui/ResponsivePreview.qml:57
#, kde-format
msgid "Category:"
msgstr "Kategorija:"

# >> @label whether the icon is scalable
#: package/contents/ui/Preview.qml:91
#: package/contents/ui/ResponsivePreview.qml:62
#, kde-format
msgid "Scalable:"
msgstr "Skalabilna:"

#: package/contents/ui/Preview.qml:92
#: package/contents/ui/ResponsivePreview.qml:63
#, kde-format
msgid "yes"
msgstr "da"

#: package/contents/ui/Preview.qml:92
#: package/contents/ui/ResponsivePreview.qml:63
#, kde-format
msgid "no"
msgstr "ne"

#: package/contents/ui/Tools.qml:118
#, kde-format
msgid "Color scheme:"
msgstr ""

#: src/main.cpp:86
#, kde-format
msgid "Start with category"
msgstr "Pokreni sa kategorijom"

#: src/main.cpp:86
#, kde-format
msgid "category"
msgstr "kategorija"

#: src/main.cpp:90
#, kde-format
msgid "Start full-screen"
msgstr "Pokreni preko celog ekrana"

#: src/main.cpp:94
#, kde-format
msgid "Run in icon-picker mode"
msgstr "Pokreni u režimu birača ikonica"
