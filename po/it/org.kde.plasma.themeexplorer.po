# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the themeexplorer package.
# Simone Solinas <ksolsim@gmail.com>, 2015.
# Luigi Toscano <luigi.toscano@tiscali.it>, 2016, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-13 00:47+0000\n"
"PO-Revision-Date: 2021-05-28 15:33+0200\n"
"Last-Translator: Luigi Toscano <luigi.toscano@tiscali.it>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.12.0\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Luigi Toscano,Simone Solinas"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "luigi.toscano@tiscali.it,ksolsim@gmail.com"

#: package/contents/ui/ColorEditor.qml:39
#, kde-format
msgid "Edit Colors"
msgstr "Modifica colore"

#: package/contents/ui/ColorEditor.qml:69
#, kde-format
msgid "Select Color"
msgstr "Seleziona colore"

#: package/contents/ui/ColorEditor.qml:111
#, kde-format
msgid "Normal text"
msgstr "Testo normale"

#: package/contents/ui/ColorEditor.qml:117
#, kde-format
msgid "Link"
msgstr "Collegamento"

#: package/contents/ui/ColorEditor.qml:121
#, kde-format
msgid "Visited Link"
msgstr "Collegamento visitato"

#: package/contents/ui/ColorEditor.qml:156
#, kde-format
msgid "Complementary colors area:"
msgstr "Area dei colori complementari:"

#: package/contents/ui/ColorEditor.qml:171
#, kde-format
msgid "Label"
msgstr "Etichetta"

#: package/contents/ui/ColorEditor.qml:194
#, kde-format
msgid "Text color:"
msgstr "Colore del testo:"

#: package/contents/ui/ColorEditor.qml:201
#, kde-format
msgid "Background color:"
msgstr "Colore di sfondo:"

#: package/contents/ui/ColorEditor.qml:208
#, kde-format
msgid "Highlight color:"
msgstr "Colore evidenziazione:"

#: package/contents/ui/ColorEditor.qml:215
#, kde-format
msgid "Link color:"
msgstr "Colore dei collegamenti:"

#: package/contents/ui/ColorEditor.qml:222
#, kde-format
msgid "Visited link color:"
msgstr "Colore dei collegamenti visitati:"

#: package/contents/ui/ColorEditor.qml:230
#, kde-format
msgid "Button text color:"
msgstr "Colore del testo dei pulsanti:"

#: package/contents/ui/ColorEditor.qml:237
#, kde-format
msgid "Button background color:"
msgstr "Colore di sfondo dei pulsanti:"

#: package/contents/ui/ColorEditor.qml:244
#, kde-format
msgid "Button mouse over color:"
msgstr "Colore dei pulsanti al passaggio del mouse:"

#: package/contents/ui/ColorEditor.qml:251
#, kde-format
msgid "Button focus color:"
msgstr "Colore dei pulsanti evidenziati"

#: package/contents/ui/ColorEditor.qml:259
#, kde-format
msgid "Text view text color:"
msgstr "Colore del testo del visore del testo:"

#: package/contents/ui/ColorEditor.qml:266
#, kde-format
msgid "Text view background color:"
msgstr "Colore di sfondo del visore del testo:"

#: package/contents/ui/ColorEditor.qml:273
#, kde-format
msgid "Text view mouse over color:"
msgstr "Colore al passaggio del mouse del visore del testo:"

#: package/contents/ui/ColorEditor.qml:280
#, kde-format
msgid "Text view focus color:"
msgstr "Colore del visore del testo evidenziato:"

#: package/contents/ui/ColorEditor.qml:288
#, kde-format
msgid "Complementary text color:"
msgstr "Colore complementare del testo:"

#: package/contents/ui/ColorEditor.qml:295
#, kde-format
msgid "Complementary background color:"
msgstr "Colore complementare dello sfondo:"

#: package/contents/ui/ColorEditor.qml:302
#, kde-format
msgid "Complementary mouse over color:"
msgstr "Colore complementare al passaggio del mouse:"

#: package/contents/ui/ColorEditor.qml:309
#, kde-format
msgid "Complementary focus color:"
msgstr "Colore complementare per evidenziazione:"

#: package/contents/ui/ColorEditor.qml:324
#: package/contents/ui/MetadataEditor.qml:137
#, kde-format
msgid "OK"
msgstr "OK"

#: package/contents/ui/ColorEditor.qml:329
#: package/contents/ui/MetadataEditor.qml:143
#, kde-format
msgid "Cancel"
msgstr "Annulla"

#: package/contents/ui/delegates/actionbutton.qml:35
#: package/contents/ui/delegates/actionbutton.qml:39
#: package/contents/ui/delegates/checkmarks.qml:31
#: package/contents/ui/delegates/checkmarks.qml:35
#: package/contents/ui/delegates/checkmarks.qml:39
#: package/contents/ui/delegates/checkmarks.qml:43
#, kde-format
msgid "Option"
msgstr "Opzione"

#: package/contents/ui/delegates/button.qml:27
#, kde-format
msgid "ToolButton"
msgstr "ToolButton"

#: package/contents/ui/delegates/button.qml:31
#: package/contents/ui/fakecontrols/Button.qml:22
#, kde-format
msgid "Button"
msgstr "Pulsante"

#: package/contents/ui/delegates/tabbar.qml:29
#, kde-format
msgid "Tab1"
msgstr "Tab1"

#: package/contents/ui/delegates/tabbar.qml:32
#, kde-format
msgid "Tab2"
msgstr "Tab2"

#: package/contents/ui/delegates/tabbar.qml:35
#, kde-format
msgid "Tab3"
msgstr "Tab3"

#: package/contents/ui/delegates/textfield.qml:28
#, kde-format
msgid "Text"
msgstr "Testo"

#: package/contents/ui/fakecontrols/CheckBox.qml:73
#, kde-format
msgid "Checkbox"
msgstr "Casella"

#: package/contents/ui/fakecontrols/LineEdit.qml:33
#, kde-format
msgid "Text input…"
msgstr "Immissione testo…"

#: package/contents/ui/main.qml:30
#, kde-format
msgid "New Theme…"
msgstr "Nuovo tema…"

#: package/contents/ui/main.qml:46
#, kde-format
msgid "Theme:"
msgstr "Tema:"

#: package/contents/ui/main.qml:58
#, kde-format
msgid "Open Folder"
msgstr "Apri cartella"

#: package/contents/ui/main.qml:63
#, kde-format
msgid "Edit Metadata…"
msgstr "Modifica metadati…"

#: package/contents/ui/main.qml:80
#, kde-format
msgid "Edit Colors…"
msgstr "Modifica colori…"

#: package/contents/ui/main.qml:96
#, kde-format
msgid "Help"
msgstr "Aiuto"

#: package/contents/ui/main.qml:101
#, kde-format
msgid "Search…"
msgstr "Cerca…"

#: package/contents/ui/main.qml:219
#, kde-format
msgid "This is a readonly, system wide installed theme"
msgstr "Questo è un tema di sistema in sola lettura "

#: package/contents/ui/main.qml:224
#, kde-format
msgid "Preview:"
msgstr "Anteprima:"

#: package/contents/ui/main.qml:239
#, kde-format
msgid "Image path: %1"
msgstr "Percorso immagine: %1"

#: package/contents/ui/main.qml:244
#, kde-format
msgid "Description: %1"
msgstr "Descrizione: %1"

#: package/contents/ui/main.qml:249
#, kde-format
msgid "Missing from this theme"
msgstr "Manca da questo tema"

#: package/contents/ui/main.qml:249
#, kde-format
msgid "Present in this theme"
msgstr "Presente in questo tema"

#: package/contents/ui/main.qml:254
#, kde-format
msgid "Show Margins"
msgstr "Mostra i margini"

#: package/contents/ui/main.qml:257
#, kde-format
msgid "Create with Editor…"
msgstr "Crea con l'editor…"

#: package/contents/ui/main.qml:257
#, kde-format
msgid "Open In Editor…"
msgstr "Apri nell'editor…"

#: package/contents/ui/MetadataEditor.qml:24
#, kde-format
msgid "New Theme"
msgstr "Nuovo tema"

#: package/contents/ui/MetadataEditor.qml:24
#, kde-format
msgid "Edit Theme"
msgstr "Modifica tema"

#: package/contents/ui/MetadataEditor.qml:59
#, kde-format
msgid "Warning: don't change author or license for themes you don't own"
msgstr ""
"Attenzione: non modificare l'autore o la licenza di temi che non sono tuoi"

#: package/contents/ui/MetadataEditor.qml:70
#, kde-format
msgid "Theme Name:"
msgstr "Nome del tema:"

#: package/contents/ui/MetadataEditor.qml:86
#, kde-format
msgid "This theme name already exists"
msgstr "Questo nome è già usato da un tema"

#: package/contents/ui/MetadataEditor.qml:95
#, kde-format
msgid "Author:"
msgstr "Autore:"

#: package/contents/ui/MetadataEditor.qml:103
#, kde-format
msgid "Email:"
msgstr "Posta elettronica:"

#: package/contents/ui/MetadataEditor.qml:111
#, kde-format
msgid "License:"
msgstr "Licenza:"

#: package/contents/ui/MetadataEditor.qml:122
#, kde-format
msgid "Website:"
msgstr "Sito web:"

#: src/main.cpp:37
#, kde-format
msgid "Plasma Theme Explorer"
msgstr "Plasma Theme Explorer"

#: src/main.cpp:41
#, kde-format
msgid "The theme to open"
msgstr "Il tema da aprire"

#~ msgid "New Theme..."
#~ msgstr "Nuovo tema..."

#~ msgid "Edit Colors..."
#~ msgstr "Modifica colori..."

#~ msgid "Ok"
#~ msgstr "Ok"
